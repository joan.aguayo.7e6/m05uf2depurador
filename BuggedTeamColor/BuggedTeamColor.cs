﻿using System;
public class Exercici2
{
    static void Main()
    {
        string color1 = Console.ReadLine();
        string color2 = Console.ReadLine();
        if (color1 == "white")
        {
            if (color2 == "green") Console.WriteLine("Team1");
            else if (color2 == "blue") Console.WriteLine("Team2");
            else if (color2 == "brown") Console.WriteLine("Team3");
        }
        else if (color1 == "red")
        {
            if (color2 == "blue")
            {
                Console.WriteLine("Team4");
            }
            else if (color2 == "black")
            {
                Console.WriteLine("Team5");
            }
            else if (color2 == "green")
            {
                Console.WriteLine("Team6");
            }
        }
        else Console.WriteLine("ERROR");
    }

}
