﻿using System;
public class Exercici3
{
    static void Main()
    {
        var result = 0.0;
       for (int i = 0; i < 10000; i++)
        {
            if (result > 100)
            {
                result = Math.Sqrt(result);
            }
            if (result < 0)
            {
                result += result * result;
            }

            result += 20.2;
        }

        //quan i=1000, al principi de les operacions el valor result és 111,56230589874906
        // i despres de les operacions queda en 30,762305898749055
        //aparentment, en cap moment result supera 120
        Console.WriteLine($"el resultat és {result}");
   }


}

